# Tagihankoperasi

Sebuah tools sederhana dengan keluaran berupa JSON API, yang akan sangat membantu developer frontend yang membuat kalkulator cicilan nasabah koperasi atau lembaga keuangan mikro lainnya untuk mem-fetch-nya.

## Requirements
Go version 1.16 linux/amd64

## Run
Pindah ke direktori tagihankoperasi.
`cd tagihankoperasi`

Bebaskan (jika terpakai) port 9900 `sudo kill -9 $(sudo lsof -t -i:9900)`

[1]. Menjalankan dari source code. 
       Jalankan `go run main.go`.
       
    atau

[2]. Menjalankan dari binary file.
       Jalankan `chmod a+x tagihankoperasi && ./tagihankoperasi`

Buka POSTMAN atau INSOMNIA,    kemudian
masukkan URL berikut. 
`http://localhost:9900/api/v1`

### RESTful service URLs
`POST /angsuran/calculate`  
dengan json body :
```
{
	"amount": "10000000.00",
	"interest": "1.83",
	"term": 36,
	"type": "FLAT",
	"start_date": "2021-04-07",
	"rounded": false
}
```

###  Hasil Pengujian CPU Utility
Program ditulis ini dengan sangat efisien. Hal ini dibuktikan dengan pada desktop yang hanya membuka editor gvim, Insomnia dan menjalankan binary go tagihankoperasi, diperoleh besaran output CPU Utility yang hanya 0.21%.



<img src="http://svelte-tutorial.unaux.com/images/backend_apps_tagihan.jpg">

Credit sumber berkas gambar: pribadi/Dwicahya Sulistyawan, developer Golang Jogjakarta

[http://svelte-tutorial.unaux.com/images/backend_apps_tagihan.jpg]



### Acknowledgement & Future Proposals
Pada versi Alpha ini implementasi calculate angsuran support pada type suku bunga pinjaman FLAT.
Tidak tertutup kemungkinan pada versi-versi berikutnya akan dikembangkan dengan tambahan support pada type suku bunga pinjaman EFEKTF, ANUITAS DAN FLOATING.

###  Pull Requests & Contribute to the Code
Monggo

### Dwicahya Sulistyawan | developer Golang Jogjakarta
