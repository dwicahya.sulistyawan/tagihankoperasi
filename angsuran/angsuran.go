package angsuran

import (
	"time"
    
	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/rupiahstuff"
)

// Due Date : tanggal jatuh tempo
// Principal : nominal pokok angsuran
// Interest : persen bunga per tahun
// Amount : jumlah angsuran (Principal [pokok] + Interest [bunga])
// Principal_Balance : sisa saldo pokok angsuran
// Total_Balance : sisa saldo total angsuran

type CclBulan struct {
	Due_Date            time.Time `json:"due_date"`
	Principal           int64     `json:"principal"`
	Interest            int64     `json:"interest"`
	Amount              int64     `json:"amount"`
	Principal_Balance   int64     `json:"principal_balance"`
	Total_Balance       int64     `json:"total_balance"`
}

type CclBulananRupiah struct {
	Due_Date            time.Time `json:"due_date"`
	Principal           string    `json:"principal"`
	Interest            string    `json:"interest"`
	Amount              string    `json:"amount"`
	Principal_Balance   string    `json:"principal_balance"`
	Total_Balance       string    `json:"total_balance"`
}

func (rm *CclBulan) UbahKeRupiah() *CclBulananRupiah {
	rmd := &CclBulananRupiah{
		Due_Date:           rm.Due_Date,
		Principal:          rupiahstuff.Sen2RupiahStr(rm.Principal),
		Interest:           rupiahstuff.Sen2RupiahStr(rm.Interest),
		Amount:             rupiahstuff.Sen2RupiahStr(rm.Amount),
		Principal_Balance:  rupiahstuff.Sen2RupiahStr(rm.Principal_Balance),
		Total_Balance:      rupiahstuff.Sen2RupiahStr(rm.Total_Balance),
	}

	return rmd
}
