package angsuran

import (
	"math"
	"math/big"
	"time"

	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/rupiahstuff"
)

const (
	nisbahHari float64 = 30.00 / 365.00
)

func CalcAngsuran(totPinjaman int64, persenBunga float64, termin int, jatuhTempo time.Time) ([]*CclBulan, error) {
	outsAwal := persenBunga * 30 / 365
	saldoPokokAngs := totPinjaman
	anuitasf := CalcAnuitas(totPinjaman, outsAwal, termin)

	pecahRM := make([]*CclBulan, termin)
	for i := 0; i < termin; i++ {
		bungaAngs := calcNilaiBunga(saldoPokokAngs, persenBunga)

		pokokAngs := calcPokokAngs(saldoPokokAngs, anuitasf, bungaAngs)

		totAngs := pokokAngs + bungaAngs
		
        // Due Date : tanggal jatuh tempo
        // Principal : nominal pokok angsuran
        // Interest : nominal bunga angsuran
        // Amount : jumlah angsuran (Principal [pokok] + Interest [bunga])
        // Principal_Balance : sisa saldo pokok angsuran

		rm := &CclBulan{
			Due_Date:           jatuhTempo,
			Principal:          pokokAngs,
			Interest:           bungaAngs,
			Amount:             totAngs,
			Principal_Balance:  saldoPokokAngs,
		}

		saldoPokokAngs = saldoPokokAngs - pokokAngs
		rm.Total_Balance = saldoPokokAngs

		jatuhTempo = jatuhTempo.AddDate(0, 1, 0)
		pecahRM[i] = rm
	}

	return pecahRM, nil
}

func calcPokokAngs(outstanding, anuitasf, i int64) int64 {
	pokokf := anuitasf - i
	if pokokf > outstanding {
		return outstanding
	}
	
	return pokokf
}

func CalcAnuitas(nilaiKini int64, outsAwal float64, tempo int) int64 {
	bigNilaiKini := big.NewFloat(float64(nilaiKini))
	bigOutsAwal := big.NewFloat(outsAwal / 100)
	besarByrBerkala := math.Pow(1+outsAwal/100, float64(-1*tempo))
    
	bigBesarByrBerkala := new(big.Float).Sub(big.NewFloat(1), big.NewFloat(besarByrBerkala))
	anuitas := new(big.Float).Mul(bigNilaiKini, bigOutsAwal)
	anuitas = anuitas.Quo(anuitas, bigBesarByrBerkala)
	intAnuitas := rupiahstuff.RoundSen(anuitas)
    
	return intAnuitas
}

func calcNilaiBunga(outstanding int64, persenBunga float64) int64 {
	bigTotPinjaman := big.NewFloat(float64(outstanding))
	bigNisbahHari := big.NewFloat(nisbahHari)
	bigPersenBunga := big.NewFloat(persenBunga / 100)

	nilaiBunga := new(big.Float).Mul(bigTotPinjaman, bigNisbahHari)
	nilaiBunga.Mul(nilaiBunga, bigPersenBunga)
	intNilaiBunga := rupiahstuff.RoundSen(nilaiBunga)
    
	return intNilaiBunga
}
