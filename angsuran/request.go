package angsuran

type NasabahRequest struct {
    // amount = Jumlah pinjaman | float 64 / Double-precision
	CrAmount           string `json:"amount"`
	// interest = Persen bunga per tahun | float 64 / Double-precision
	Interest           string `json:"interest"`
	// term = Termin | int
	Term               int    `json:"term"`
	// type : baru diimplementasikan 1 jenis type dlm releases alpha ini yakni FLAT
	Type               string `json:"type"`
	// start_date = Tanggal Mulai Angsuran Awal | date
	StartDate          string `json:"start_date,omitempty"`
	// round = Pembulatan ke integer | bool
	Rounded            bool   `json:"rounded"`
}
