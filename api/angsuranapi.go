package api

import (
	"log"
    "time"
	"net/http"
	"encoding/json"
	"strconv"

	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/rupiahstuff"

	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/angsuran"
)

// JalankanCalcAngsuran : Initialisasi API
func JalankanCalcAngsuran(rt *http.ServeMux) {
	rt.HandleFunc("/api/v1/angsuran/calculate", hitCalcAngsuran)
	rt.HandleFunc("/api/v1/anuitas/calculate", hitCalcAnuitas)
}

func hitCalcAnuitas(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		jsonErrorResponse("Hanya mendukung metode HTTP POST saja.", http.StatusMethodNotAllowed, w)
		return
	}

	d := json.NewDecoder(r.Body)
	var masukan angsuran.NasabahRequest
	err := d.Decode(&masukan)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Kesalahan yang tidak terduga terjadi ketika men-decode request: "+err.Error(), http.StatusBadRequest, w)
		return
	}

	floatTotPinjaman, err := strconv.ParseFloat(masukan.CrAmount, 64)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Besaran pinjaman tidak valid.", http.StatusBadRequest, w)
		return
	}

	totPinjamanSen := int64(floatTotPinjaman * 100.00)

	persenBunga, err := strconv.ParseFloat(masukan.Interest, 64)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Besaran pinjaman tidak valid.", http.StatusBadRequest, w)
		return
	}

	outsAwal := persenBunga * 30 / 365

	anuitasf := angsuran.CalcAnuitas(totPinjamanSen, outsAwal, masukan.Term)
	anuitasRupiah := rupiahstuff.Sen2RupiahStr(anuitasf)

	apiResp := struct {
		Annuity string `json:"annuity"`
	}{
		Annuity: anuitasRupiah,
	}

	jsonResponse(apiResp, http.StatusOK, w)
}

func hitCalcAngsuran(w http.ResponseWriter, r *http.Request) {
    
    const langgamSolo = "2006-01-02"
    
	if r.Method != http.MethodPost {
		jsonErrorResponse("Hanya mendukung metode HTTP POST saja.", http.StatusMethodNotAllowed, w)
		return
	}

	d := json.NewDecoder(r.Body)
	var masukan angsuran.NasabahRequest
	err := d.Decode(&masukan)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Kesalahan yang tidak terduga terjadi ketika men-decode request: "+err.Error(), http.StatusBadRequest, w)
		return
	}

	// Memparsing total nilai pinjaman ke dalam float 64 atau Double-precision (CrAmount)
	floatTotPinjaman, err := strconv.ParseFloat(masukan.CrAmount, 64)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Besaran pinjaman tidak valid.", http.StatusBadRequest, w)
		return
	}
	// Konversi ke int64, notasi + sen
	intTotPinjaman := int64(floatTotPinjaman * 100.00)

	// Memparsing persen bunga per tahun ke dalam float 64 atau Double-precision (Interest)
	persenBunga, err := strconv.ParseFloat(masukan.Interest, 64)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Besaran pinjaman tidak valid.", http.StatusBadRequest, w)
		return
	}
    
	// Memparsing tanggal awal mulai angsuran (StartDate) -- pada studi kasus Kop SKD tgl default 2020-10-01
	tglMulaiCcl, err := time.Parse(langgamSolo, masukan.StartDate)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Langgam tanggal tidak valid. Masukkan tanggal dengan langgam YYYY MM DD.", http.StatusBadRequest, w)
		return
	}

	// Calculate Angsuran
	pecahRM, err := angsuran.CalcAngsuran(intTotPinjaman, persenBunga, masukan.Term, tglMulaiCcl)
	if err != nil {
		log.Println(err)
		jsonErrorResponse("Kesalahan terjadi dalam proses menghitung cicilan pinjaman.", http.StatusInternalServerError, w)
		return
	}

	pecahRM_D := make([]*angsuran.CclBulananRupiah, len(pecahRM))
	for i, v := range pecahRM {
		pecahRM_D[i] = v.UbahKeRupiah()
	}

	jsonResponse(pecahRM_D, http.StatusOK, w)
}
