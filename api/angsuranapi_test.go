package api_test

import (
    "fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"encoding/json"
	"bytes"
	"reflect"
	"testing"

	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/rupiahstuff"
    
	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/angsuran"
)

func TestJalankanCalcAngsuran(t *testing.T) {
	rt := http.NewServeMux()
	api.JalankanCalcAngsuran(rt)

	srv := httptest.NewServer(rt)
	defer srv.Close()

	data := map[string]interface{}{
		"amount":       "10000000.00",
		"interest":     "1.36",
		"term":         36,
		"start_date":   "2020-10-01",
	}

	b := new(bytes.Buffer)
	err := json.NewEncoder(b).Encode(data)
	if err != nil {
		t.Fatalf("Tidak dapat meng-encode data: %v", err)
	}

	res, err := http.Post(fmt.Sprintf("%s/api/v1/angsuran/calculate", srv.URL), "application/json", b)
	if err != nil {
		t.Fatalf("Tidak dapat mengirim POST Request:  %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		t.Errorf("Status tidak OK; status yang berjalan: %v", res.Status)
	}

	d := json.NewDecoder(res.Body)
	var angsResp []angsuran.CclBulananRupiah
	err = d.Decode(&angsResp)
	if err != nil {
		t.Fatalf("Tidak dapat men-decode response Body: %v", err)
	}

	var ExpData []angsuran.CclBulananRupiah
	err = json.Unmarshal([]byte(expDataStr), &ExpData)
	if err != nil {
		t.Fatalf("Tidak dapat men-decode results yang diharapkan: %v", err)
	}

	dataCheck := reflect.DeepEqual(angsResp, ExpData)
	if !dataCheck {
		log.Println(angsResp)
		log.Println("abc")
		log.Println(ExpData)
		t.Fatalf("Response tidak sesuai dengan result yang diharapkan.")
	}
}

func TestCalcAngsuranMethodNotAllowed(t *testing.T) {
	rt := http.NewServeMux()
	api.JalankanCalcAngsuran(rt)

	srv := httptest.NewServer(rt)
	defer srv.Close()

	res, err := http.Get(fmt.Sprintf("%s/api/v1/angsuran/calculate", srv.URL))
	if err != nil {
		t.Fatalf("Tidak dapat mendeteksi Request :  %v", err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("Status yang semestinya: Method-Not-Allowed. Status yang diperoleh:  %v", res.Status)
	}
}
