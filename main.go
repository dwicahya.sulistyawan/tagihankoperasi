package main

import (
	"log"
	"net/http"
	"time"
    
	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/api"
)

func main() {
	rt := http.NewServeMux()
	api.JalankanCalcAngsuran(rt)

	s := &http.Server{
		Addr:           ":9900",
		Handler:        rt,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Fatal(s.ListenAndServe())
}
