package rupiahstuff

import (
	"math/big"
	"strconv"
)

func RoundSen(sen *big.Float) int64 {
	senFloat := new(big.Float).Add(sen, big.NewFloat(0.5))
	senInt, _ := senFloat.Int64()
	return senInt
}

func Sen2RupiahStr(sen int64) string {
	strSen := strconv.FormatInt(sen, 10)
	lstrSen := len(strSen)
	if lstrSen > 2 {
		strSen = strSen[:lstrSen-2] + "." + strSen[lstrSen-2:]
	} else if lstrSen > 1 {
		strSen = "0." + strSen
	} else {
		strSen = "0.0" + strSen
	}
	return strSen
}
