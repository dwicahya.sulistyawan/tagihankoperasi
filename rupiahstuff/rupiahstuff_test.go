package rupiahstuff_test

import (
	"math/big"
	"testing"

	"gitea.com/dwicahya.sulistyawan/tagihankoperasi/rupiahstuff"
)

func TestRoundSen(t *testing.T) {
	ttSen := []struct {
		nama        string
		pecahansen  *big.Float
		r_sen       int64
	}{
		{"1 sen rupiah", big.NewFloat(1), 1},
		{"1.5 sen rupiah", big.NewFloat(1.5), 2},
		{"1.6 sen rupiah", big.NewFloat(1.6), 2},
		{"1.0 sen rupiah", big.NewFloat(1.0), 1},
		{"1.0 sen rupiah", big.NewFloat(1.167), 1},
		{"1.0 sen rupiah", big.NewFloat(1.678), 2},
	}

	for _, tSen := range ttSen {
		t.Run(tSen.nama, func(t *testing.T) {
			rc := rupiahstuff.RoundSen(tSen.pecahansen)
			if rc != tSen.r_sen {
				t.Fatalf("Diharapkan %v , yang diperoleh: %v", tSen.r_sen, rc)
			}
		})
	}
}

func TestSen2RupiahStr(t *testing.T) {
	ttSen := []struct {
		nama        string
		pecahansen  int64
		rupiahstr   string
	}{
		{"1 sen rupiah", 1, "0.01"},
		{"10 sen rupiah", 10, "0.10"},
		{"1 rupiah", 100, "1.00"},
		{"10 rupiah", 1000, "10.00"},
		{"1234 sen rupiah", 1234, "12.34"},
		{"12345 sen rupiah", 12345, "123.45"},
	}

	for _, tSen := range ttSen {
		t.Run(tSen.nama, func(t *testing.T) {
			ds := rupiahstuff.Sen2RupiahStr(tSen.pecahansen)
			if ds != tSen.rupiahstr {
				t.Fatalf("Diharapkan %v untuk %v , yang diperoleh: %v", tSen.rupiahstr, tSen.pecahansen, ds)
			}
		})
	}
}
